angular.module('seti').controller('HomeController', ['$scope', 'cicloService', function ($scope, cicloService) {

        $scope.title = 'Bienvenidos a mi aplicación!';
        $scope.text = 'AngularJS-Java';
        $scope.ciclo = {};
        $scope.mostrarCiclos = false;
        
        /**
         * Consulta todos los ciclos
         */
        $scope.consultarCiclos = function () {
            $scope.mostrarCiclos = true;
            cicloService.GetCiclos().then(
                    function (success) {
                        if (success.status == 200) {
                            $scope.ciclos = success.data;
                        } else {
                            console.log('Error');
                        }
                    }, function (error) {
                console.log('Error');
            });
        }
        
        /**
         * Adiciona un nuevo ciclo
         */        
        $scope.adicionarCiclo = function() {
            cicloService.PostCiclo($scope.ciclo).then(
                    function (success) {
                        if (success.status == 204) {
                            alert('Se creó correctamente el ciclo: ' + $scope.ciclo.nombre);
                            $scope.consultarCiclos();
                        } else {
                            console.log('Error');
                        }
                    }, function (error) {
                console.log('Error');
            });
        }        
       
        /**
         * Asigna el ciclo seleccionado
         * @paramCiclo
         */
        $scope.seleccionarCiclo = function (paramCiclo) {
            $scope.ciclo = paramCiclo;
        }

        /**
         * Actualiza el ciclo
         */
        $scope.actualizarCiclo = function () {
            cicloService.UpdateCiclo($scope.ciclo).then(
                    function (success) {
                        if (success.status == 204) {
                            alert('Se actualizó correctamente el ciclo: ' + $scope.ciclo.nombre);
                            $scope.consultarCiclos();
                        } else {
                            console.log('Error');
                        }
                    }, function (error) {
                console.log('Error');
            });
        }
        
        /**
         * Elimina el ciclo
         */
        $scope.eliminarCiclo = function (paramCiclo) {
            cicloService.DeleteCiclo(paramCiclo).then(
                    function (success) {
                        if (success.status == 204) {
                            alert('Se eliminó correctamente el ciclo: ' + paramCiclo.nombre);
                            $scope.consultarCiclos();
                        } else {
                            console.log('Error');
                        }
                    }, function (error) {
                console.log('Error');
            });
        }

    }]);