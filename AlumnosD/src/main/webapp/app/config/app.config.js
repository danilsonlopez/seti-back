angular.module('seti').config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider){
    
    $locationProvider.hashPrefix('');
    
    $routeProvider
        .when("/", {
            templateUrl: 'app/components/home/home.html',
            controller: 'HomeController'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);

