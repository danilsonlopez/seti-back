angular.module('seti').service('cicloService', function ($http, $rootScope) {
    /**
     * Tomar todos los ciclos
     */
    this.GetCiclos = function () {
        return $http.get("http://localhost:8080/AlumnosD-1.0-SNAPSHOT/webresources/com.mycompany.alumnosd.ciclo/");
    }

    /**
     * Actualizar un ciclo
     */
    this.UpdateCiclo = function (paramCiclo) {
        return $http.put("http://localhost:8080/AlumnosD-1.0-SNAPSHOT/webresources/com.mycompany.alumnosd.ciclo/" + paramCiclo.pkId, paramCiclo);
    }
    
    /**
     * Adicionar un ciclo
     */
    this.PostCiclo = function (paramCiclo) {
        return $http.post("http://localhost:8080/AlumnosD-1.0-SNAPSHOT/webresources/com.mycompany.alumnosd.ciclo/", paramCiclo);
    }
    
    /**
     * Eliminar un ciclo
     */
    this.DeleteCiclo = function (paramCiclo) {
        return $http.delete("http://localhost:8080/AlumnosD-1.0-SNAPSHOT/webresources/com.mycompany.alumnosd.ciclo/" + paramCiclo.pkId);
    }
});